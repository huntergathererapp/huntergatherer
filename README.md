# huntergatherer

## Usage

### Build
```bash
docker compose build
```

### Run
```bash
docker compose up
```

The slowest service to start is `huntergatherer-play`. When that service is
running, it is ready to view.

### View
https://localhost:8080


